/** @type {import('tailwindcss').Config} */
module.exports = {
  // purge: [],
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: 'media', // or 'media' or 'class'
  theme: {
    container: {
      center: true
    },
    extend: {
      maxHeight: {
        'banner': '465px',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
